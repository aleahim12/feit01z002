import numpy as np
from scipy import signal as sig
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go


letter_zeta = 'ζ'
letter_omega = 'ω'
letter_sigma = 'σ'
subscript_n = 'ₙ'

simulation_time = 30
pole_real_range = [-5, 3]
pole_imag_range = [0, 10]
slider_margin = 20

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H2(children='Отскочен одѕив на преносна функција'),
    html.Div([
        html.Div([dcc.Graph(id='pole_map')], style={'width': '30%', 'display': 'inline-block'}),
        html.Div([dcc.Graph(id='step_response')], style={'width': '70%', 'display': 'inline-block'}),
    ]),

    dcc.Tabs(id='tabs', children=[
        dcc.Tab(label='Систем од прв ред', value='first_order_system', children=[
            html.Div(id='coefficient_a_label'),
            html.Div([
                dcc.Slider(
                    id='coefficient_a',
                    min=pole_real_range[0],
                    max=pole_real_range[1],
                    step=0.05,
                    value=-1,
                    marks={i: '{}'.format(i) for i in range(pole_real_range[0], 1 + pole_real_range[1])},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
        ]),
        dcc.Tab(label='Систем од втор ред, апериодичен', value='second_order_system_overdamped', children=[
            html.Div([
                html.Div(id='coefficient_a1_label'),
                html.Div([dcc.Slider(
                    id='coefficient_a1',
                    min=pole_real_range[0],
                    max=pole_real_range[1],
                    step=0.05,
                    value=-1,
                    marks={i: '{}'.format(i) for i in range(pole_real_range[0], 1 + pole_real_range[1])},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
                html.Div(id='coefficient_a2_label'),
                html.Div([dcc.Slider(
                    id='coefficient_a2',
                    min=pole_real_range[0],
                    max=pole_real_range[1],
                    step=0.05,
                    value=-1,
                    marks={i: '{}'.format(i) for i in range(pole_real_range[0], 1 + pole_real_range[1])},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
            ]),
        ]),
        dcc.Tab(label='Систем од втор ред, осцилаторен', value='second_order_system_underdamped', children=[
            html.Div([
                html.Div(id='coefficient_sigma_label'),
                html.Div([dcc.Slider(
                    id='coefficient_sigma',
                    min=pole_real_range[0],
                    max=pole_real_range[1],
                    step=0.05,
                    value=-1,
                    marks={i: '{}'.format(i) for i in range(pole_real_range[0], 1 + pole_real_range[1])},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
                html.Div(id='coefficient_omega_label'),
                html.Div([dcc.Slider(
                    id='coefficient_omega',
                    min=pole_imag_range[0],
                    max=pole_imag_range[1],
                    step=0.05,
                    value=1,
                    marks={i: '{}'.format(i) for i in range(pole_imag_range[0], 1 + pole_imag_range[1])},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
            ]),
        ]),
        dcc.Tab(label='Систем од втор ред преку {} и {}'.format(letter_zeta, letter_omega + subscript_n),
                value='second_order_system_zeta_omega_n', children=[
            html.Div([
                html.Div(id='coefficient_zeta_label'),
                html.Div([dcc.Slider(
                    id='coefficient_zeta',
                    min=0,
                    max=2,
                    step=0.01,
                    value=0.1,
                    marks={i/10: '{}'.format(i/10) for i in range(0, 20)},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
                html.Div(id='coefficient_omega_n_label'),
                html.Div([dcc.Slider(
                    id='coefficient_omega_n',
                    min=0,
                    max=10,
                    step=0.05,
                    value=1,
                    marks={i: '{}'.format(i) for i in range(0, 11)},
                    updatemode='drag',
                )], style={'margin-bottom': slider_margin,
                           'margin-left': slider_margin,
                           'margin-right': slider_margin}),
            ]),
        ]),
    ]),
    dcc.Checklist(id='kp', options=[{'label': 'Процесно засилување Kp = 1', 'value': 'Kp'}], values=['Kp']),
    html.Div(id='transfer_function_info'),
])


@app.callback(
    dash.dependencies.Output('pole_map', 'figure'),
    [
        dash.dependencies.Input('tabs', 'value'),
        dash.dependencies.Input('coefficient_a', 'value'),
        dash.dependencies.Input('coefficient_a1', 'value'),
        dash.dependencies.Input('coefficient_a2', 'value'),
        dash.dependencies.Input('coefficient_sigma', 'value'),
        dash.dependencies.Input('coefficient_omega', 'value'),
        dash.dependencies.Input('coefficient_zeta', 'value'),
        dash.dependencies.Input('coefficient_omega_n', 'value'),
     ])
def update_figure(tab_id, a, a1, a2, sigma, omega, zeta, omega_n):
    system = get_system(tab_id, a, a1, a2, sigma, omega, zeta, omega_n, [])
    if not system:
        return
    x = [pole.real for pole in system.poles]
    y = [pole.imag for pole in system.poles]
    pole_style = {'size': 20, 'symbol': 'x'}
    poles_trace = go.Scatter(x=x, y=y, mode='markers', marker=pole_style, name='Полови', showlegend=True)
    traces = [poles_trace]
    return {
        'data': traces,
        'title': 'Местоположба на половите на комплексната рамнина',
        'layout': go.Layout(
            xaxis={'range': [-12, 3], 'title': 'Реална оска'},
            yaxis={'range': [-12, 12], 'title': 'Имагинарна оска'},
        )
    }


@app.callback(
    dash.dependencies.Output('step_response', 'figure'),
    [
        dash.dependencies.Input('tabs', 'value'),
        dash.dependencies.Input('coefficient_a', 'value'),
        dash.dependencies.Input('coefficient_a1', 'value'),
        dash.dependencies.Input('coefficient_a2', 'value'),
        dash.dependencies.Input('coefficient_sigma', 'value'),
        dash.dependencies.Input('coefficient_omega', 'value'),
        dash.dependencies.Input('coefficient_zeta', 'value'),
        dash.dependencies.Input('coefficient_omega_n', 'value'),
        dash.dependencies.Input('kp', 'values'),
    ])
def update_figure(tab_id, a, a1, a2, sigma, omega, zeta, omega_n, kp_values):
    system = get_system(tab_id, a, a1, a2, sigma, omega, zeta, omega_n, kp_values)
    if not system:
        return
    time, step_response = sig.step(system, T=np.arange(0, simulation_time, 0.05))
    input_trace = {'x': time, 'y': np.ones_like(step_response), 'name': 'Отскочен влез'}
    step_response_trace = {'x': time, 'y': step_response, 'name': 'Отскочен одѕив'}
    traces = [input_trace, step_response_trace]
    response_upper_limit = 1.2 if tab_id in ['first_order_system', 'second_order_system_overdamped'] else 2
    return {
        'data': traces,
        'title': 'Отскочен одѕив',
        'layout': go.Layout(
            xaxis={'range': [0, simulation_time], 'title': 'Време'},
            yaxis={'range': [0, response_upper_limit], 'title': 'Одѕив'},
        )
    }


@app.callback(
    dash.dependencies.Output('coefficient_a_label', 'children'),
    [dash.dependencies.Input('coefficient_a', 'value')])
def display_value(value):
    return 'Полот на преносната фукнција, a = {}'.format(value)


@app.callback(
    dash.dependencies.Output('coefficient_a1_label', 'children'),
    [dash.dependencies.Input('coefficient_a1', 'value')])
def display_value(value):
    return 'Пол на преносната фукнција, a1 = {}'.format(value)


@app.callback(
    dash.dependencies.Output('coefficient_a2_label', 'children'),
    [dash.dependencies.Input('coefficient_a2', 'value')])
def display_value(value):
    return 'Пол на преносната фукнција, a2 = {}'.format(value)


@app.callback(
    dash.dependencies.Output('coefficient_sigma_label', 'children'),
    [dash.dependencies.Input('coefficient_sigma', 'value')])
def display_value(value):
    return 'Реалниот дел од комплексниот пол, {} = {}'.format(letter_sigma, value)


@app.callback(
    dash.dependencies.Output('coefficient_omega_label', 'children'),
    [dash.dependencies.Input('coefficient_omega', 'value')])
def display_value(value):
    return 'Имагинарниот дел од комплексниот пол, {} = {}'.format(letter_omega, value)


@app.callback(
    dash.dependencies.Output('coefficient_zeta_label', 'children'),
    [dash.dependencies.Input('coefficient_zeta', 'value')])
def display_value(value):
    return 'Коефициентот на придушување, {} = {}'.format(letter_zeta, value)


@app.callback(
    dash.dependencies.Output('coefficient_omega_n_label', 'children'),
    [dash.dependencies.Input('coefficient_omega_n', 'value')])
def display_value(value):
    return 'Природната фрекфенција на преносната функција, {} = {}'.format(letter_omega + subscript_n, value)


@app.callback(
    dash.dependencies.Output('transfer_function_info', 'children'),
    [
        dash.dependencies.Input('tabs', 'value'),
        dash.dependencies.Input('coefficient_a', 'value'),
        dash.dependencies.Input('coefficient_a1', 'value'),
        dash.dependencies.Input('coefficient_a2', 'value'),
        dash.dependencies.Input('coefficient_sigma', 'value'),
        dash.dependencies.Input('coefficient_omega', 'value'),
        dash.dependencies.Input('coefficient_omega_n', 'value'),
        dash.dependencies.Input('kp', 'values')
     ])
def display_value(tab_id, a, a1, a2, sigma, omega, omega_n, kp_values):
    kp = 'Kp' in kp_values
    tf_text = ''
    gain = 0
    try:
        if tab_id == 'first_order_system':
            tf_text = '{} / (s - a)'.format('-a' if kp else '1')
            gain = 1 if kp else -1/a
        elif tab_id == 'second_order_system_overdamped':
            tf_text = '{0} / ((s - a1)(s - a2))'.format('a1*a2' if kp else '1')
            gain = 1 if kp else 1 / (a1*a2)
        elif tab_id == 'second_order_system_underdamped':
            kp_text = '({}**2 + {}**2)'.format(letter_sigma, letter_omega) if kp else '1'
            tf_text = '{0} / ((s - {1} - j*{2})(s - {1} + j*{2}))'.format(kp_text, letter_sigma, letter_omega)
            gain = 1 if kp else 1 / (sigma**2 + omega**2)
        elif tab_id == 'second_order_system_zeta_omega_n':
            kp_text = '{}**2'.format(letter_omega + subscript_n) if kp else 1
            tf_text = '{0} / (s**2 + 2*{1}*{2}*s + {2}**2)'.format(kp_text, letter_zeta, letter_omega + subscript_n)
            gain = 1 if kp else 1 / omega_n**2
    except ZeroDivisionError:
        gain = float('inf')

    return 'Преносната функција G(s) = {}. Процесното засилување Kp = {}'.format(tf_text, gain)


def get_system(tab_id, a, a1, a2, sigma, omega, zeta, omega_n, kp_values):
    kp = 'Kp' in kp_values
    if tab_id == 'first_order_system':
        return sig.TransferFunction(-a if kp else 1, [1, -a])
    elif tab_id == 'second_order_system_overdamped':
        return sig.TransferFunction(a1*a2 if kp else 1, [1, -(a1 + a2), a1*a2])
    elif tab_id == 'second_order_system_underdamped':
        return sig.TransferFunction(sigma**2 + omega**2 if kp else 1, [1, -2*sigma, sigma**2 + omega**2])
    elif tab_id == 'second_order_system_zeta_omega_n':
        return sig.TransferFunction(omega_n ** 2 if kp else 1, [1, 2 * zeta * omega_n, omega_n ** 2])


if __name__ == '__main__':
    app.run_server()
